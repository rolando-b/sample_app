require 'test_helper'

class CompanyControllerTest < ActionDispatch::IntegrationTest
  test "should get redirected when not logged in" do
    get companies_path
    assert_redirected_to login_url
  end

end
