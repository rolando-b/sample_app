require 'test_helper'

class ProductControllerTest < ActionDispatch::IntegrationTest

  
  test "should redirect when not logged in" do
    get products_path
    assert_redirected_to login_url
  end

end
