require 'test_helper'

class CompanyRegisterTest < ActionDispatch::IntegrationTest
  test "invalid register information" do
    get companies_path
    #If the POST request is bad, we check that the user hasn't been added (ie we still have the same number of users)
    assert_no_difference 'Company.count' do
      post companies_path, params: { company: { name:  "",
                                         nb_employees: "",
                                         city: "",
                                         turnover:  "",
                                         phone: "" } }
    end
  end
  
   test "valid signup information" do
    get companies_path
    #If the POST request is ok, we expect a count difference of 1 in the number or users
    assert_difference 'Company.count', 1 do
      post companies_path, params: { company: { name:  "Company",
                                         nb_employees: "10",
                                         city: "London",
                                         turnover:  "10000000",
                                         phone: "7817817397" } }
    end
  end
end
