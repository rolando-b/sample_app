require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  
  test "invalid signup information" do
    get signup_path
    #If the POST request is bad, we check that the user hasn't been added (ie we still have the same number of users)
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name:  "",
                                         email: "user@invalid",
                                         company_id: 0,
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'users/new'
  end
  
   test "valid signup information" do
    get signup_path
    #If the POST request is ok, we expect a count difference of 1 in the number or users
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name:  "Example User",
                                         email: "user@example.com",
                                         company_id: 3,
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
  end
end
