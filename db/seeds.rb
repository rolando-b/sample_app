# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name:  "Admin",
             email: "admin@website.org",
             password:              "administrator",
             password_confirmation: "administrator",
             company_id: 1,
             admin: true)


Company.create!(name:  "Pro-Duct",
             nb_employees:1,
             city: "Compiègne",
             phone: "0781781739",
             turnover: 100000000)
            
30.times do |n|
  name  = Faker::Company.name
  nb_employees = Faker::Company.duns_number
  city = Faker::Address.city
  phone = Faker::PhoneNumber.phone_number
  turnover = rand(1000...300000000)
  
  Company.create!(name:  name,
             nb_employees: nb_employees,
             city: city,
             phone: phone,
             turnover: turnover)
end

60.times do |n|
  name  = Faker::Name.name
  email = Faker::Internet.safe_email
  password = "password"
  company_id = rand(1...30)
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               company_id: company_id)
end

61.times do |n|
  name  = Faker::Commerce.product_name
  description = Faker::Hacker.say_something_smart
  price = Faker::Commerce.price
  company_id = rand(1...30)
  Product.create!(name:  name,
               description: description,
               price: price,
               company_id: company_id)
end