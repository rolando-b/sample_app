class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.integer :nb_employees
      t.string :city
      t.integer :turnover

      t.timestamps
    end
  end
end
