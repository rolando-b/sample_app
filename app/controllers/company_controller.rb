class CompanyController < ApplicationController
  before_action :logged_in_user, only: [:index]
  def new
    @company= Company.new
  end
  
  def show
    @company = Company.find(params[:id])
    @users = User.where(company_id: params[:id])
  end
  
  def index
    @companies = Company.paginate(page: params[:page])
  end
  
  def indexApi
    @companies = Company.paginate(page: params[:page])
    render json: @companies
  end
  
  def create
    @company = Company.new(company_params)
    if @company.save
      flash[:success] = "Successfully created a company"
      redirect_to signup_path
    else
      render 'new'
    end
  end
  
  # When destroying a company, all users and products are destroyed
  def destroy
    User.where(company_id: params[:id]).destroy_all
    Product.where(company_id: params[:id]).destroy_all
    Company.find(params[:id]).destroy
    flash[:success] = "Company deleted"
    redirect_to companies_path
  end
  
  
  private

    def company_params
      params.require(:company).permit(:name,:nb_employees, :city, :turnover,:phone)
    end
  
end
