class ProductController < ApplicationController
  before_action :logged_in_user, only: [:new, :index]
  
  def new
    @product= Product.new
    @company= Company.find_by(id: current_user.company_id)
  end
  
  def show
    @product = Product.find(params[:id])
  end
  
  def index
    @products = Product.paginate(page: params[:page])
  end
  
  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = "Successfully created a product"
      redirect_to products_path
    else
      render 'new'
    end
  end
  
  
  def showApi
    @products = Product.where(company_id: params[:company_id])
    render json: @products
  end
  
  def destroy
    Product.find(params[:id]).destroy
    flash[:success] = "Product deleted"
    redirect_to products_path
  end
  
  private

    def product_params
      params.require(:product).permit(:name, :description, :price, :company_id)
    end
  
end
