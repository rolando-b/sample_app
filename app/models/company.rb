class Company < ApplicationRecord
  validates :name, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
  validates :nb_employees, presence: true
  validates :city, presence: true
  validates :phone, presence: true

  
end
