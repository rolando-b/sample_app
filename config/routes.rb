Rails.application.routes.draw do
  
  get 'product/new'

  get 'company/new'

  root   'static_pages#home'
  get    '/help',    to: 'static_pages#help'
  get    '/about',   to: 'static_pages#about'
  get    '/contact', to: 'static_pages#contact'
  get    '/companies', to: 'company#index'
  get    '/create_company', to: 'company#new'
  post   '/companies', to: 'company#create'
  get    '/api/companies', to: 'company#indexApi'
  get    '/products', to: 'product#index'
  get    '/new_product', to: 'product#new'
  post   '/products', to: 'product#create'
  get    '/api/products', to: 'product#showApi'
  get    '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users
  resources :company
  resources :product
 
end
